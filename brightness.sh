#!/bin/sh

step=0.1;
target=;

main() {
    data=$(xrandr --verbose)
    displays=$(echo "${data}" | egrep " connected " | awk '{print $1}')

    for display in ${displays} ; do
        current=$(echo "${data}" | egrep "${display}|Brightness" | egrep -A1 "${display}" | grep Brightness | awk '{print $NF}')

        if [ -n "${target}" ] ; then
            new=${target};
            echo "${display}: ${current} ----> ${new}"
        else
            new=$(awk "BEGIN{new=${current}+${step};if(new<0)new=0; print new}")
            echo "${display}: ${current} --{${step}}--> ${new}"
        fi;

        xrandr --output "${display}" --brightness "${new}"

        :;
    done;
}





case ${1} in
    \+) step=${step};;
    -) step=$(awk "BEGIN{print ${step}*(-1)}");;
    *) 
        echo "${1}"| egrep -q "^\+[0-9.]+$" && {
            step=${1##*+};
        } || {
            echo "${1}" | egrep -q "^-[0-9.]+$" && {
                step=${1};
            } || {
                echo "${1}" | egrep -q "^[0-9.]+$" && {
                    target=${1}
                }
            }
        }
        
        ;;
esac;

main;
